# Parse authors in `Cargo.toml` and similar

This parses names in the format `Name <email> (url)` used in `Cargo.toml` metadata (also in npm).

This crate tries to parse any field in any order and to fix common errors to salvage as much data as possible.

To read the authors key from `Cargo.toml` you'll need [`cargo_toml`](https://lib.rs/cargo_toml) crate. Once you have the author names, parse them by calling `Author::new(string)`.
